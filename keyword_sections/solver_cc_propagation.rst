
solver cc propagation
---------------------

Keywords related to time-dependent coupled cluster propagation settings.

Required keywords
^^^^^^^^^^^^^^^^^

.. container:: sphinx-custom

   ``integrator: [string]``
   
   Specifies the integration method used for real-time propagation. Required.

   Valid keyword values are:
   
   - ``rk4`` Fourth-order Runge-Kutta (RK4)     
   - ``gl2`` Second-order Gauss-Legendre (GL2)
   - ``gl4`` Fourth-order Gauss-Legendre (GL4)
   - ``gl6`` Sixth-order Gauss-Legendre (GL6)

.. container:: sphinx-custom

   ``initial time: [real]``
   
   The start time for the real-time propagation given in atomic units. Required.

.. container:: sphinx-custom

   ``final time: [real]``
   
   The end time for the real-time propagation given in atomic units. Required.

.. container:: sphinx-custom

   ``time step: [real]``
   
   Size of the time steps for the real-time propagation given in atomic units. Required.

Optional keywords
^^^^^^^^^^^^^^^^^

.. container:: sphinx-custom

   ``steps between output: [integer]``
   
   Default: :math:`1`

   Specifies how many time steps the solver should take between each time output (energy, dipole moment, ...) is  written to file. Optional.

.. container:: sphinx-custom

   ``implicit treshold: [real]``
   
   Default: :math:`10^{-11}` (or 1.0d-11)

   Specifies how tightly the Euclidian norm of the residual of implicit Runge-Kutta methods should converge before going to the next time step. Optional.

.. container:: sphinx-custom

   ``energy output``
   
   Default: ``false``

   Write energy to file every ``steps between output``. Optional.

.. container:: sphinx-custom

   ``dipole moment output``
   
   Default: ``false``

   Write dipole moment to file every ``steps between output``. Optional.

.. container:: sphinx-custom

   ``electric field output``
   
   Default: ``false``

   Write electric field to file every ``steps between output``. Optional.

.. container:: sphinx-custom

   ``amplitudes output``
   
   Default: ``false``

   Write cluster amplitudes to file every ``steps between output``. Optional.

.. container:: sphinx-custom

   ``multipliers output``
   
   Default: ``false``

   Write multipliers to file every ``steps between output``. Optional.

.. container:: sphinx-custom

   ``density matrix output``
   
   Default: ``false``

   Write molecular orbital (MO) density matrix to file every ``steps between output``. Optional.
