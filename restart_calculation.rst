.. _restart-calc:

Restarting calculations
=======================

Restart is possible for all the SCF and coupled cluster solvers in eT.

Global restart
--------------

To restart every part of a calculation,
the ``restart`` keyword can be specified in the
:ref:`do section <do-section>`.
The restart information will then be read from file,
if eT finds the required files to restart a section.
Default start guesses will be used,
in case the files cannot be found.
The converged :math:`t` amplitudes are used
as a starting guess for the ground state multipliers.
If available,
the converged right excited states are used for
the left excited states (and vice versa),
otherwise the orbital differences are used.


To improve the convergence behavior,
one might want to restart a CC3 calculation from
a converged CCSD calculation.
The example below shows how CC3 excitation energies are calculated
by restarting from a CCSD calculation.

The minimal input to obtain two right excited states with CCSD
would look like this:

.. code-block:: none

   system
      name: H2O He
      charge: 0
   end system

   do
      excited state
   end do

   method
      hf
      ccsd
   end method

   solver cc es
      singlet states: 2
   end solver cc es

   geometry
   basis: cc-pVDZ
    H          0.86681        0.60144        5.00000
    H         -0.86681        0.60144        5.00000
    O          0.00000       -0.07579        5.00000
    He         0.10000       -0.02000        7.53000
   end geometry

For the CC3 input the :ref:`method section <method-section>`
has to be changed from ccsd to cc3 and the global restart
keyword needs to be added to the :ref:`do section <do-section>`:

.. code-block:: none

   method
     hf
     cc3
   end method

   do
     restart
     excited state
   end do

The first input will be saved as ``ccsd.inp`` and the second as ``cc3.inp``.
There are **two** ways to restart using the launch script.
Both inputs can be handed to the launch script in the same call:

.. code-block:: none

   path/to/eT_launch.py ccsd.inp cc3.inp

At first ``ccsd.inp`` will be executed in the scratch directory,
directly followed by the execution of ``cc3.inp`` in the same scratch directory.
eT will find the restart information from CCSD,
as the same scratch directory is used for both inputs.

**Alternatively**,
``ccsd.inp`` can be run first,
saving its restart information using the ``-save`` flag
and specifying a path,
see the :ref:`eT_launch<eT_launch>` page for details.

.. code-block:: none

   path/to/eT_launch.py ccsd.inp -save ./

There will be a directory called ``ccsd_restart_files`` containing
the restart information after the calculation has finished,
because we used the submit directory as save directory.
To restart CC3 from these restart files we type the following command:

.. code-block:: none

   path/to/eT_launch.py cc3.inp -load ./ccsd_restart_files

The :ref:`launch<eT_launch>` script will then copy the files from ``ccsd_restart_files``
to the scratch directory and run ``cc3.inp``.

Specific restart
----------------

The ``restart`` keyword can also be specified
in the sections of the respective solvers
if restart is only desired for specific parts of the calculation.
The sections with a restart keyword are:

- :ref:`solver scf <scf-section>`
- :ref:`solver scf geoopt <scf-geoopt-section>`
- :ref:`solver cc gs <cc-gs-section>`
- :ref:`solver cc multipliers <cc-multipliers-section>`
- :ref:`solver cc es <cc-es-section>`

If all restart keywords are set,
the effect is the same as if the global restart keyword was specified.

Restart of multilevel coupled cluster calculations
--------------------------------------------------

For MLCC calculations, there are some special restart keywords that enter the
:ref:`mlcc section <mlcc-section>`:

- ``orbital restart``
- ``cnto restart`` and ``nto restart``

The ``orbital restart`` keyword enables restart from partitioned orbitals
from a previous MLCC calculation.

.. note::

   When the ``orbital restart`` keyword is used the orbital partitioning
   is skipped and the orbitals on file are assumed to be suitable
   for the restarted calculation.
   The keyword is not necessary for general MLCC restart,
   as the orbital partitioning step will yield the same orbitals
   if the input for the orbitals is the same.
   This keyword can be used in cases when this step is computationally expensive.

If CNTOs or NTOs/canonical are requested,
the ``cnto restart`` or ``nto restart`` keyword can be used to avoid
the lower level CC calculation necessary to construct these orbitals:
In that case,
the eigenvectors of :math:`\boldsymbol{M}` and :math:`\boldsymbol{N}`
(the CNTO/NTO transformation matrices) obtained in a previous
MLCC calculation are read from disk.

.. note::

   The ``cnto restart`` and ``nto restart`` keywords are only referenced
   if the orbital type is ``cnto``/``cnto-approx`` or ``nto-canonical``,
   respectively.

Restarting from eT v1.0 or v1.1
-------------------------------

Restarting from eT v1.0 or v1.1 requires special care since the format of the restart files was changed.
However, the ``convert_v1.0_restart_files.py`` script in ``dev_tools``
can convert old restart files to the new format.
The easiest way to do this is to use the ``-load-v1-0, --load-v1-0-restart-directory`` option
with :ref:`eT_launch.py <eT_launch>`.
This will convert the old restart files and write them to scratch.
The calculation can then be restarted normally using the ``restart`` keywords.

.. note::
   The sign convention of the orbitals was changed between v1.0 and v1.1.
   When restarting coupled cluster calculations from v1.0,
   the keyword ``skip`` must be used in the ``solver scf`` section
   to ensure that the signs are consistent:

   .. code-block:: none

      solver scf
         skip
         ...
      end solver scf

.. warning::

   MLCC calculations cannot be restarted from v1.0 or v1.1.

It is also possible to convert the old files manually using the script.

.. code-block:: none

   python convert_v1_0_restart_files.py path/to/old/restart/directory path/to/new/restart/directory

The second path argument is optional.
If it is not provided,
a new restart directory will be created in the old.
