.. _visualization:

Visualizing orbitals and densities
==================================

Currently eT is able to produce ``.plt`` and ``.cube`` files for canonical molecular orbitals,
RHF densities, and CC densities.

The ``.plt`` or ``.cube`` files can then be read by a visualization program like Chimera.

Plotting in RHF:
-----------------

Plotting is enabled by specifying the :ref:`visualization section <plot-section>`.
For instance, to plot the canonical MOs 10, 11 and 12 and the HF density,
the following section is added:

.. code-block:: none

   visualization
      plot hf orbitals: [10,12]
      plot hf density
   end visualization

The grid can be adapted by specifying the ``grid spacing`` and the ``grid buffer``.
Note that the files can become quite large.


Plotting CC Densities:
----------------------

The coupled cluster ground state density can be plotted from
``mean value`` and ``response`` calculations using the ``plot cc density`` keyword.

Plotting of transition densities and excited state densities is enabled with
``plot transition densities`` and ``plot es densities`` respectively.
By default all densities for the states, specified in ``initial states`` are plotted.
This can be changed using the ``states to plot`` keyword and a list of states
in the the ``visualization`` section.

To request the plotting of the densities for the ground state and first excited state
the following visualization section can be used:

.. code-block:: none

   visualization
      plot cc density
      plot transition densities
      plot es densities
      states to plot: {0,1}
   end visualization


Molden input:
-------------

eT can also write a ``molden input`` file containing the geometry,
basis set, and orbital coefficients which can be visualized using
`Molden <https://www3.cmbi.umcn.nl/molden/>`_.

This file is requested from :ref:`Solver SCF <scf-section>` using ``write molden``.
