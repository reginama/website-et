.. _required-sections:

Required input sections
=======================

There are four required input sections for eT: 

- ``system``
- ``method``
- ``do`` 
- ``geometry``

.. include:: keyword_sections/system.rst
.. include:: keyword_sections/method.rst
.. include:: keyword_sections/do.rst
.. include:: keyword_sections/geometry.rst
