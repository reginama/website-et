.. _qed-calc:

QED calculation
===============

Setting up an QED-HF calculation
--------------------------------

A QED-Hartree-Fock (HF) calculation is performed by specifying a QED-HF (``qed-hf``) wavefunction 
in the :ref:`method section <method-section>` of the input file. To start a ground state
QED-HF we specify

.. code-block:: none

   method
      qed-hf
   end method

   do
      ground state
   end do

A minimal working example for a QED-HF single-point calculation on water with wavevector
:math:`\vec{k}=(0,0,1)`, frequency :math:`\omega=0.5` au and coupling :math:`\lambda=0.05` au:

.. code-block:: none

   system
      name: H2O
      charge: 0
   end system

   do
      ground state
   end do

   method
      qed-hf
   end method

   qed
      modes: 2
      wavevector: {0.0, 0.0, 1.0}
      frequency: {0.5, 0.5}
      coupling: {0.05, 0.05}
   end qed

   geometry
   basis: cc-pVDZ
   H          0.86681        0.60144        0.00000
   H         -0.86681        0.60144        0.00000
   O          0.00000       -0.07579        0.00000
   end geometry
  
Save this as ``h2o.inp`` and invoke the :ref:`launch<eT_launch>` script.

.. code-block:: none

   path/to/eT_launch.py h2o.inp

After the calculation finished you should find ``h2o.out`` and ``h2o.timing.out`` 
in your working directory. If the calculation exited successfully 
(look for ``eT terminated successfully!`` at the bottom of the file), 
a summary of the calculation should be printed at the end.



Relevant input sections
-----------------------

:ref:`Solver SCF <scf-section>`

:ref:`HF mean value <hf-mean-value-section>`

:ref:`QED <qed-section>`
